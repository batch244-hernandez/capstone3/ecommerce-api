import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import ProductView from './pages/ProductView';

import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AdminDashboard from "./pages/AdminDashboard";
import Users from "./pages/Users";
import Orders from "./pages/Orders";

import './App.css';
import { UserProvider } from './UserContext';


function App() {

    // Global state hook for the user information for validating if a user is logged in
    const [user, setUser] = useState({
      id: null,
      isAdmin: null
    });

    // Function for clearing localStorage on logout
    const unsetUser = () => {
      localStorage.clear();
    }

    // Used to check if the user information is properly stored upon login ang localStorage is cleared upon logout
    useEffect(() => {
      console.log(user);
      console.log(localStorage);
    }, [user])


    const retrieveUserDetails = (token) => {

      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      })
    }

    if(localStorage.getItem("token") !== null && user.id === null){
      retrieveUserDetails(localStorage.getItem("token"))
    }

  return (
    // In React JS, multiple components rendered in a single component should be wrapped in a parent component
    // Fragment ensures that an error wil be prevented

    // We store information in the context by providing the information using the "UserProvider" component and passing the information via the "value" prop
    // All information inside the value prop will be accessible to pages/components wrapped around the UserProvider.
    // It allows re-rendering when the "value" prop changes.
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/products" element={<Products/>}/>
              <Route path="/products/:productId" element={<ProductView/>}/>              
              <Route id="login" path="/login" element={<Login/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/logout" element={<Logout/>}/>
              <Route path="*" element={<Error/>}/>
              <Route path="/admin" element={<AdminDashboard />} />
              <Route path="/admin/users" element={<Users/>} />
              <Route path="/admin/orders" element={<Orders/>} />
          </Routes>
        </Container>        
      </Router>
    </UserProvider>
    
  );
}

export default App;