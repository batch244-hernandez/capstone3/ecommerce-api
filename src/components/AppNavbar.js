import { useContext } from 'react';
import { Navbar, Container, Nav} from 'react-bootstrap';
import { MDBIcon } from 'mdb-react-ui-kit';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar() {

  const { user } = useContext(UserContext);

  // State to store the user information stored in the login page
  // const [user, setUser] = useState(localStorage.getItem('email'));
  // console.log(user);

	return(
		<Navbar expand="lg">
        <Container>
          <Navbar.Brand as={Link} to="/" style={{color: "white"}}>Millie Mouse</Navbar.Brand>
          <MDBIcon fas icon="mouse fa-2x me-auto" style={{color: "white"}}/>
          <Navbar.Toggle aria-controls="basic-navbar-nav"><MDBIcon fas icon="bars" id="bar-icon"/></Navbar.Toggle>
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
              <Nav.Link as={NavLink} to="/" style={{color: "white"}} >Home</Nav.Link>
              
              {
                (user.isAdmin)
                ?
                <Nav.Link as={ NavLink } to="/admin" end style={{color: "white"}} >Admin Dashboard</Nav.Link>
                :   
                <Nav.Link as={ NavLink } to="/products" end style={{color: "white"}} >Products</Nav.Link>

              }

              {/*Conditional rendering to such that the Logout link will be shown instead of the Login and Register when a user is logged in*/}
              {(user.id !== null)

                ?
                <>          
                  <Nav.Link as={NavLink} to="/logout" style={{color: "white"}} >Logout</Nav.Link>
                </>


                :
                <>
                  <Nav.Link as={NavLink} to="/login" style={{color: "white"}} >Login</Nav.Link>
                  {/*<Nav.Link as={NavLink} to="/register">Register</Nav.Link> */}
                </>
              }

            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
	)
}

// export default AppNavbar (can also be used)