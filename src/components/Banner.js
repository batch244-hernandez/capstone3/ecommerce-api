import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

	const { title, content, details, destination, label } = data;	

	return (					
		<div className="p-5 text-center" id="main">
			<div className="main-text">
				<h6 style={{color: "white"}}>
					{content}
				</h6>
				<h1 style={{color: "white"}}>
					{title}
				</h1>
				<p style={{color: "white"}}><em>{details}</em></p>
				<Button as={Link} to={destination} variant="primary">{label}</Button>
			</div>
		</div>
	)
}