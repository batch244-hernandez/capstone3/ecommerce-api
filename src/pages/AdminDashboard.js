import {useEffect, useState} from "react";
import { Button, Table, Modal, Form, FloatingLabel, Offcanvas, Card, ListGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";

// import UserContext from "../UserContext";

export default function AdminDashboard(){

	// const { user } = useContext(UserContext);

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	// Create allProducts state to contain the products from the response of our fetch data.
	const [allProducts, setAllProducts] = useState([]);

	// State hooks to store the values of the input fields for our modal.
	const [productId, setProductId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

    // State to determine whether submit button in the modal is enabled or not
    const [isActive, setIsActive] = useState(false);

    // State for Add/Edit Modal
    const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

	// Functions to toggle the opening and closing of the "Add Product" modal
	const openAdd = () => setShowAdd(true); //Will show the modal
	const closeAdd = () => setShowAdd(false); //Will hide the modal

	/* 
	Function to open the "Edit Product" modal:
		- Fetches the selected product data using the product ID
		- Populates the values of the input fields in the modal form
		- Opens the "Edit Product" modal
	*/
	// We have passed a parameter from the edit button so we can retrieve a specific product and bind it with our input fields.
	const openEdit = (id) => {
		setProductId(id);

		// Getting a specific product to pass on the edit modal
		fetch(`${ process.env.REACT_APP_API_URL }/products/${id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			// updating the product states for editing
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		});

		setShowEdit(true)
	};

	/* 
	Function to close our "Edit Product" modal:
		- Reset from states back to their initial values
		- Empties the input fields in the form whenever the modal is opened for adding a product
	*/
	const closeEdit = () => {

		// Clear input fields upon closing the modal
	    setName('');
	    setDescription('');
	    setPrice(0);
		setShowEdit(false);
	};

	// Retrieve all products
	// fetchData() function to get all the active/inactive products.
	const fetchData = () =>{
		// get all the products from the database
		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return (
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						{/*<td>{product.isActive ? "Active" : "Inactive"}</td>*/}
						<td>
					    	<span className="badge badge-success rounded-pill d-inline" style={{color:"black"}}>{product.isActive ? "Active" : "Inactive"}</span>
					  	</td>
						<td>
							{
								// conditonal rendering on what button should be visible based on the status of the product
								(product.isActive)
								?
									<Button variant="danger" size="sm" className="grow" onClick={() => archive(product._id, product.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(product._id, product.name)}>Unarchive</Button>
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
									</>

							}
						</td>
					</tr>
				)
			}));
		});
	}

	// to fetch all products in the first render of the page.
	useEffect(()=>{
		fetchData();
	});


	// Deactivate a product
	const archive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data) {
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${productName} is now inactive.`
				});
				// To show the update with the specific operation intiated.
				fetchData();

			} else {
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// Reactivate product
	const unarchive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data) {

				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${productName} is now active.`
				});
				// To show the update with the specific operation intiated.
				fetchData();

			} else {

				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// Create product
	const addProduct = (e) =>{
			
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/products`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
				    price: price			    
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){

		    		// To automatically add the update in the page
		    		fetchData();

		    		Swal.fire({
		    		    title: "Product successfully Added",
		    		    icon: "success",
		    		    text: `${name} is now added`
		    		});
		    		

		    		// Reset all states to their initial values
					// Provides better user experience by clearing all the input fieles when the user adds another product
		    		setName('');
		    		setDescription('');
		    		setPrice(0);

		    		// Automatically close the modal
		    		closeAdd();

		    	} else {

		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}

		    })

		    
	}

	// Update Product Information
	const updateProduct = (e) =>{
			
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
		    	method: "PUT",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
				    price: price	   
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product successfully Updated",
		    		    icon: "success",
		    		    text: `${name} is now updated`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the form
		    		closeEdit();

		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});

		    		closeEdit();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setPrice(0);
	} 

	// Submit button validation for add/edit product
	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
        if(name !== "" && description !== "" && price > 0){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price]);

	return(
		// (user.isAdmin)
		// ?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				{/*Adding a new product */}
				<Button variant="success" className="
				mx-2" onClick={openAdd}>Add Product</Button>
				<Button className=""variant="primary" onClick={handleShow}>
		      	Show Admin Dashboard Navigation
		    	</Button>
		    	<Offcanvas className=""show={show} onHide={handleClose} backdrop="static">
		    	    <Offcanvas.Header closeButton className="btn-dark">
		    	      <Offcanvas.Title>Admin Dashboard</Offcanvas.Title>
		    	    </Offcanvas.Header>
		    	    <Offcanvas.Body>
		    	        <Card style={{ width: '18rem' }} className="bg-dark">
		    	          <ListGroup variant="dark">
		    	            <ListGroup.Item as={Link} to="/admin/users">Users</ListGroup.Item>
		    	            {/*<ListGroup.Item as={Link} to="/admin/orders">Orders</ListGroup.Item>*/}
		    	         </ListGroup>
		    	        </Card>
		    	    </Offcanvas.Body>
		    	</Offcanvas>
			</div>

			<h2 className="mt-5 mb-3 text-center">Products</h2>
			{/*For viewing all the products in the database.*/}			
		    <Table className="table align-middle mb-0" variant="dark" striped bordered hover>
		      <thead>
		        <tr>
		          <th>Product ID</th>
		          <th>Product Name</th>
		          <th>Description</th>
		          <th>Price</th>
		          <th>Status</th>
		        </tr>
		      </thead>
		      <tbody>
		      	{allProducts}
		      </tbody>
		    </Table>
			{/*End of table for product viewing*/}

	    	{/*Modal for Adding a new product*/}
	        <Modal show={showAdd} fullscreen={true} onHide={closeAdd}>
	    		<Form onSubmit={e => addProduct(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Add New Product</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="name" className="mb-3">
	    	                <FloatingLabel
		              		 controlId="floatingProductName"
		                       label="Product Name"
		                       className="mb-3">
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Product Name" 
	    		                value = {name}
	    		                onChange={e => setName(e.target.value)}
	    		                required
	    	                />
	    	                </FloatingLabel>
	    	            </Form.Group>

	    	            <Form.Group controlId="description" className="mb-3">
	    	                <FloatingLabel
		              		 controlId="floatingProductDescription"
		                       label="Product Description"
		                       className="mb-3">
	    	                <Form.Control
	    	                	as="textarea"
	    	                	rows={3}
	    		                placeholder="Product Description" 
	    		                value = {description}
	    		                onChange={e => setDescription(e.target.value)}
	    		                required
	    	                />
	    	                </FloatingLabel>
	    	            </Form.Group>

	    	            <Form.Group controlId="price" className="mb-3">
	    	                <FloatingLabel
		              		 controlId="floatingPrice"
		                       label="Price"
		                       className="mb-3">
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Price" 
	    		                value = {price}
	    		                onChange={e => setPrice(e.target.value)}
	    		                required
	    	                />
	    	                </FloatingLabel>
	    	            </Form.Group>	 
    	           
	    			</Modal.Body>

	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>	
	    	</Modal>
	    {/*End of modal for adding product*/}

    	{/*Modal for Updating a product*/}
        <Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
    		<Form onSubmit={e => updateProduct(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Update a Product</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
    	        	<Form.Group controlId="name" className="mb-3">
    	                <FloatingLabel
		              		 controlId="floatingProductName"
		                       label="Product Name"
		                       className="mb-3">
    	                <Form.Control 
    		                type="text" 
    		                placeholder="Product Name" 
    		                value = {name}
    		                onChange={e => setName(e.target.value)}
    		                required
    	                />
    	                </FloatingLabel>
    	            </Form.Group>

    	            <Form.Group controlId="description" className="mb-3">    	
    	                <FloatingLabel
		              		 controlId="floatingProductDescription"
		                       label="Product Description"
		                       className="mb-3">
    	                <Form.Control
    	                	as="textarea"
    	                	rows={3}
    		                placeholder="Product Description" 
    		                value = {description}
    		                onChange={e => setDescription(e.target.value)}
    		                required
    	                />
    	                </FloatingLabel>
    	            </Form.Group>

    	            <Form.Group controlId="price" className="mb-3">
    	                <Form.Label>Product Price</Form.Label>
    	                <FloatingLabel
		              		 controlId="floatingPrice"
		                       label="Price"
		                       className="mb-3">
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Price" 
    		                value = {price}
    		                onChange={e => setPrice(e.target.value)}
    		                required
    	                />
    	                </FloatingLabel>
    	            </Form.Group>
    			</Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>
	    		</Form>	
	    	</Modal>
		</>
	)
}
