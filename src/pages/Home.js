
import Banner from '../components/Banner';
// import { Carousel } from 'react-bootstrap';
import { MDBCarousel, MDBCarouselItem } from 'mdb-react-ui-kit';


export default function Home() {

	const data = {
		content: "Take control.",
		title: "MILLIE MOUSE",
		details: "-Buy stuff, kaching, lil' skkkrrrr, then we're done, yeah?-",
		destination: "/products",
		label: "Buy now!"
	}
	return (
		<>
			<Banner data={data}/> 
			{/*<Carousel fade>
			    <Carousel.Item>
			        <img
			          className="d-block w-100"
			          src="https://assets.reedpopcdn.com/razer-basilisk-v3-pro_availablein_desktop_1920x700.jpg/BROK/resize/1200x1200%3E/format/jpg/quality/70/razer-basilisk-v3-pro_availablein_desktop_1920x700.jpg"
			          alt="First slide"
			        />
			        <Carousel.Caption>
			          <h3>NEW REYNA EMPRESS PRO</h3>
			          <p>We win. We survive.</p>
			        </Carousel.Caption>
			    </Carousel.Item>

			    <Carousel.Item>
			        <img
			          className="d-block w-100"
			          src="https://images.saymedia-content.com/.image/t_share/MTc0NDU2NzU5MzIxODMxMDQ2/budget-gaming-mouse.jpg"
			          alt="Second slide"
			        />

			        <Carousel.Caption>
			          <h3>JETT BLADE STORM</h3>
			          <p> I got your backs. Just...y'know...from the front.</p>
			        </Carousel.Caption>
			    </Carousel.Item>
			</Carousel>*/}


			<MDBCarousel showIndicators>
			    <MDBCarouselItem
			        className='w-100 d-block rounded-3'
			        itemId={1}
			        src='https://assets.reedpopcdn.com/razer-basilisk-v3-pro_availablein_desktop_1920x700.jpg/BROK/resize/1200x1200%3E/format/jpg/quality/70/razer-basilisk-v3-pro_availablein_desktop_1920x700.jpg'
			        alt='First slide'
			    >
			        <h5>NEW REYNA EMPRESS PRO</h5>
			        <p>We win. We survive.</p>
			    </MDBCarouselItem>
			    <MDBCarouselItem
			        className='w-100 d-block rounded-3'
			        itemId={2}
			        src='https://images.saymedia-content.com/.image/t_share/MTc0NDU2NzU5MzIxODMxMDQ2/budget-gaming-mouse.jpg'
			        alt='Second slide'
			    >
			        <h5>JETT BLADE STORM</h5>
			        <p>I got your backs. Just...y'know...from the front.</p>
			    </MDBCarouselItem>			    
			</MDBCarousel>
	      	         
        </>
	)
}