import { useState, useEffect, useContext } from 'react';
import { ListGroup, Form } from 'react-bootstrap';
import { MDBBtn, MDBContainer, MDBCard, MDBCardBody, MDBCardImage, MDBRow, MDBCol, MDBIcon, MDBInput } from 'mdb-react-ui-kit';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function Login() {

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function authenticate(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data.access !== undefined){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Login successful!"
				});
			} else {
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again!"
				})
			}
		})

		setEmail('');
		setPassword('');
	}


	// Retrieving user details

	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {

		if ((email !== '' && password !== '')) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}

		}, [email, password]);

	return (

		(user.isAdmin === true)
		?
			<Navigate to='/admin'/>
		:
			(user.id !== null)
			?
				<Navigate to='/products'/>
			:
			<MDBContainer className="my-5">
				<Form className="mt-5" style={{width:"100%"}} onSubmit={(e) => authenticate(e)}>
					<MDBCard>
						<MDBRow className='g-0'>

							<MDBCol md='6'>
								<MDBCardImage src='https://cdn-icons-png.flaticon.com/512/6119/6119641.png' fluid/>
							</MDBCol>

							<MDBCol md='6'>
								<MDBCardBody className='d-flex flex-column'>

									<div className='d-flex flex-row mt-2'>
										<MDBIcon fas icon="mouse fa-3x me-3"/>
										<span className="h1 fw-bold mb-0">Millie Mouse</span>
									</div>

									<h5 className="fw-normal my-4 pb-3" style={{letterSpacing: '1px'}}>Sign in to your account</h5>

										<MDBInput wrapperClass='mb-4' label='Email address' id='formControlLg' type='email' size="lg" value={email} onChange={e => setEmail(e.target.value)}/>
										<MDBInput wrapperClass='mb-4' label='Password' id='formControlLg' type='password' size="lg" value={password} onChange={e => setPassword(e.target.value)}/>
									{isActive
									?
									<MDBBtn className="mb-4 px-5" color='primary' size='lg'>Login</MDBBtn>
									:
									<MDBBtn className="mb-4 px-5" color='primary' size='lg' disabled>Login</MDBBtn>
									}
									<span className="mb-1 pb-lg-2" style={{color: '#393f81'}}> Don't have an account?</span>
									<ListGroup.Item as={Link} to="/register" id="register" style={{color: '#393f81'}}>Sign up here</ListGroup.Item>
								</MDBCardBody>
							</MDBCol>

						</MDBRow>
					</MDBCard>
				</Form>

			</MDBContainer>
	)
}