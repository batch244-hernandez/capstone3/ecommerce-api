import {useContext, useEffect, useState} from "react";

import { Table } from "react-bootstrap";

import UserContext from "../UserContext";

export default function Orders(){

	const { user } = useContext(UserContext);

	const [allOrders, setAllOrders] = useState([]);

	// Retrieve all users
	const fetchData = () =>{
		// get all orders from the database
		fetch(`${process.env.REACT_APP_API_URL}/users/allOrders`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllOrders(data.map(users => {
				return (
					<tr key={user._id}>
						<td>{user._id}</td>
						<td>{user.orders}</td>
					</tr>
				)
			}));
		});
	}

	// to fetch all users in the first render of the page.
	useEffect(()=>{
		fetchData();
	});


	// // Set as admin
	// const setAsAdmin = (id, userName) =>{
	// 	console.log(id);
	// 	console.log(userName);

	// 	// Using the fetch method to set the isActive property of the product document to false
	// 	fetch(`${process.env.REACT_APP_API_URL}/users/${id}/setAsAdmin`, {
	// 		method: "PATCH",
	// 		headers:{
	// 			"Content-Type": "application/json",
	// 			"Authorization": `Bearer ${localStorage.getItem("token")}`
	// 		},
	// 		body: JSON.stringify({
	// 			isAdmin: true
	// 		})
	// 	})
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		console.log(data)

	// 		if(data) {
	// 			Swal.fire({
	// 				title: "Set admin successful!",
	// 				icon: "success",
	// 				text: "User is now an admin."
	// 			});
	// 			// To show the update with the specific operation intiated.
	// 			fetchData();

	// 		} else {
	// 			Swal.fire({
	// 				title: "Oops!",
	// 				icon: "error",
	// 				text: "Something went wrong. Please try again later!"
	// 			});
	// 		}
	// 	})
	// }

	// // Set as Not Admin
	// const setAsNotAdmin = (id, userName) =>{
	// 	console.log(id);
	// 	console.log(userName);

	// 	// Using the fetch method to set the isActive property of the product document to false
	// 	fetch(`${process.env.REACT_APP_API_URL}/users/${id}/setAsAdmin`, {
	// 		method: "PATCH",
	// 		headers:{
	// 			"Content-Type": "application/json",
	// 			"Authorization": `Bearer ${localStorage.getItem("token")}`
	// 		},
	// 		body: JSON.stringify({
	// 			isAdmin: false
	// 		})
	// 	})
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		console.log(data)

	// 		if(data) {

	// 			Swal.fire({
	// 				title: "Set to not admin successful!",
	// 				icon: "success",
	// 				text: "User set to not an admin."
	// 			});
	// 			fetchData();

	// 		} else {

	// 			Swal.fire({
	// 				title: "Oops!",
	// 				icon: "error",
	// 				text: "Something went wrong. Please try again later!"
	// 			});
	// 		}
	// 	})
	// }

	return(
		<>			
			<h2 className="mt-5 mb-3 text-center">Orders</h2>
			<Table striped bordered hover>
		      <thead>
		        <tr>
		          <th>User ID</th>
		          <th>Orders</th>
		        </tr>
		      </thead>
		      <tbody>
	        	{allOrders}
		      </tbody>
		    </Table>	    	
		</>
	)

}

