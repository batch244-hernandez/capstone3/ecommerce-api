import { useState, useEffect, useContext } from 'react';
import { Form } from 'react-bootstrap';
import { MDBBtn, MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody, MDBCardImage, MDBInput, MDBIcon } from 'mdb-react-ui-kit';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();
	
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');	

	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);


	function registerUser(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true){
				Swal.fire({
					title: "Registration successful",
					icon: "success",
					text: "Lorem ipsum dolor sit amet!"
				})

				setEmail('');
				setPassword1('');
				setPassword2('');

			} else {
				Swal.fire({
					title: "User already exists",
					icon: "error",
					text: "Please provide another email to complete the registration."
				})
			}
			navigate("/login");				
		})
	}

	useEffect(() => {
		if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2]);

	return (
		(user.id !== null)
		?
			<Navigate to="/products"/>
			
		:

		<MDBContainer fluid>
			<Form className="mt-5" style={{width:"100%"}} onSubmit={(e) => registerUser(e)}>
		      <MDBCard className='text-black m-5' style={{borderRadius: '25px'}}>
		        <MDBCardBody>
		          <MDBRow>
		            <MDBCol md='10' lg='6' className='order-2 order-lg-1 d-flex flex-column align-items-center'>

		              <p className="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Sign up</p>

		              <div className="d-flex flex-row align-items-center mb-4">
		                <MDBIcon fas icon="envelope me-3" size='lg'/>
		                <MDBInput label='Email Address' id='form1' type='email' value={email} onChange={e => setEmail(e.target.value)}/>
		              </div>

		              <div className="d-flex flex-row align-items-center mb-4">
		                <MDBIcon fas icon="lock me-3" size='lg'/>
		                <MDBInput label='Password' id='form2' type='password' value={password1} onChange={e => setPassword1(e.target.value)}/>
		              </div>

		              <div className="d-flex flex-row align-items-center mb-4">
		                <MDBIcon fas icon="key me-3" size='lg'/>
		                <MDBInput label='Verify Password' id='form3' type='password' value={password2} onChange={e => setPassword2(e.target.value)}/>
		              </div>

		              {isActive
		              ?
		              	<MDBBtn variant="success" type="submit" id="submitBtn" className='mb-4' size='lg'>Register</MDBBtn>
		              :
		              	<MDBBtn variant="danger" type="submit" id="submitBtn" className='mb-4' size='lg' disabled>Register</MDBBtn>
		              }
		            </MDBCol>

		            <MDBCol md='10' lg='6' className='order-1 order-lg-2 d-flex align-items-center'>
		              <MDBCardImage src='https://cdn-icons-png.flaticon.com/512/6119/6119585.png' fluid/>
		            </MDBCol>

		          </MDBRow>
		        </MDBCardBody>
		      </MDBCard>
		    </Form>
		</MDBContainer>

		// // <Row>
		// // 	<Col lg={{ span: 6, offset: 3}}>
		// <Card className = "Cardregister col-lg-5">
		// 	 <img className = "imageRegister image-fluid" src ="https://e7.pngegg.com/pngimages/652/136/png-clipart-minnie-mouse-illustration-minnie-mouse-mickey-mouse-logo-of-mickey-mouse-head-presentation-mouse-thumbnail.png" alt=''/>
		// 		<Form className="mt-5 formregister" style={{width:"100%"}} onSubmit={(e) => registerUser(e)}>

		// 			<Form.Group controlId = "userEmail">
		// 				<FloatingLabel
		// 		              		 controlId="floatingEmail"
		// 		                       label="Email Address"
		// 		                       className="mb-3">
		// 				<Form.Control 
		// 					type="email"
		// 					placeholder="Email Address"
		// 					required
		// 					value={email}
		// 					onChange={e => setEmail(e.target.value)}
		// 				/>
		// 				</FloatingLabel>
		// 			</Form.Group>

		// 			<Form.Group controlId = "password1">
		// 				<FloatingLabel
		// 		              		 controlId="floatingPassword1"
		// 		                       label="Password"
		// 		                       className="mb-3">
		// 				<Form.Control 
		// 					type="password"
		// 					placeholder="Password"
		// 					required
		// 					value={password1}
		// 					onChange={e => setPassword1(e.target.value)}
		// 				/>
		// 				</FloatingLabel>
		// 			</Form.Group>

		// 			<Form.Group className="mb-3" controlId = "password2">
		// 				<FloatingLabel
		// 		              		 controlId="floatingPassword2"
		// 		                       label="Verify Password"
		// 		                       className="mb-3">
		// 				<Form.Control					 
		// 					type="password"
		// 					placeholder="Verify Password"
		// 					required
		// 					value={password2}
		// 					onChange={e => setPassword2(e.target.value)}
		// 				/>
		// 				</FloatingLabel>
		// 			</Form.Group>
					
		// 			{isActive
		// 				?
		// 					<Button variant="success" type="submit" id="submitBtn">Submit</Button>
		// 				: 	
		// 					<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
		// 			}
		// 		</Form>
		// </Card>
		// // 	</Col>
		// // </Row>

	)
}