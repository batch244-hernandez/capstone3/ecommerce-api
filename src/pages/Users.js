import { useEffect, useState} from "react";

import { Button, Table} from "react-bootstrap";
import Swal from "sweetalert2";

// import UserContext from "../UserContext";

export default function Users(){

	// const { user } = useContext(UserContext);

	const [allUsers, setAllUsers] = useState([]);

	// Retrieve all users
	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllUsers(data.map(user => {
				return (
					<tr key={user._id}>
						<td>{user._id}</td>
						<td>{user.email}</td>
						<td>{user.password}</td>
						<td>{user.isAdmin ? "Admin" : "Not admin"}</td>
						<td>
							{
								// conditonal rendering on what button should be visible based on the status of the product
								(!user.isAdmin)
								?
									<Button variant="danger" size="sm" onClick={() => setAsAdmin(user._id, user.name)}>Set as Admin</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => setAsNotAdmin(user._id, user.name)}>Set as Not Admin</Button>
									</>
							}
						</td>
					</tr>
				)
			}));
		});
	}

	// to fetch all users in the first render of the page.
	useEffect(()=>{
		fetchData();
	});


	// Set as admin
	const setAsAdmin = (id, userName) =>{
		console.log(id);
		console.log(userName);

		// Using the fetch method to set the isAdmin property of the user document to true
		fetch(`${process.env.REACT_APP_API_URL}/users/${id}/setAsAdmin`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data) {
				Swal.fire({
					title: "Set admin successful!",
					icon: "success",
					text: "User is now an admin."
				});
				// To show the update with the specific operation intiated.
				fetchData();

			} else {
				Swal.fire({
					title: "Oops!",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// Set as Not Admin
	const setAsNotAdmin = (id, userName) =>{
		console.log(id);
		console.log(userName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/users/${id}/setAsAdmin`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAdmin: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data) {

				Swal.fire({
					title: "Set to not admin successful!",
					icon: "success",
					text: "User set to not an admin."
				});
				fetchData();

			} else {

				Swal.fire({
					title: "Oops!",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	return(
		<>			
			<h1 className="mt-5 mb-3 text-center" style={{color:"white"}}>Users</h1>
			<Table className="table align-middle mb-0" variant="dark" striped bordered hover>
		      <thead>
		        <tr>
		          <th>User ID</th>
		          <th>Email</th>
		          <th>Password</th>
		          <th>Status</th>
		        </tr>
		      </thead>
		      <tbody>
	        	{allUsers}
		      </tbody>
		    </Table>	    	
		</>
	)

}

